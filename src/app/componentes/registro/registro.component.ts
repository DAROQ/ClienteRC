import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutenticacionService } from '../../servicios/autenticacion.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  public usuario = {
    usuario: "",
    contrasena: "",    
    nombres: "",
    apellidos: "",
    correo: ""
  };

  constructor(private autenticacionService:AutenticacionService, private router:Router) { }

  ngOnInit() {
  }

  public registrar() {    
    this.autenticacionService.registrarUsuario(this.usuario.usuario,this.usuario.contrasena,this.usuario.nombres,this.usuario.apellidos,this.usuario.correo).subscribe(data => {
      console.log(data);
      this.router.navigateByUrl('/').then(()=>{
        alert("Se ha registrado Satisfactoriamente");
      });      
    });
  }

}
