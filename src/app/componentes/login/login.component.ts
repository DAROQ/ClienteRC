import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutenticacionService } from '../../servicios/autenticacion.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public usuario = {
    usuario:"",
    contrasena:""
  }

  constructor(private autenticacionService:AutenticacionService, private router:Router) { }

  ngOnInit() {
  }

  public iniciarSesion() {
    console.log(this.usuario);
    this.autenticacionService.iniciarSesion(this.usuario.usuario,this.usuario.contrasena).subscribe(data => {
      console.log(data);
      localStorage.setItem("token",data.token);
      this.router.navigateByUrl("/").then(()=>{
        alert("Bienvenido(a) "+data.nombre);
      })
    });
  }

}
