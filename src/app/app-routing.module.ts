import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistroComponent } from './componentes/registro/registro.component';
import { LoginComponent } from './componentes/login/login.component';
import { PedidoComponent } from './componentes/pedido/pedido.component';
import { ResultadoComponent } from './componentes/resultado/resultado.component';


const routes: Routes = [
  { path: 'registro', component: RegistroComponent },
  { path: 'login' , component: LoginComponent},
  { path: 'pedido' , component: PedidoComponent},
  { path: 'resultado' , component: ResultadoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
