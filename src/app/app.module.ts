import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

// Componentes de la Aplicación
import { AppComponent } from './app.component';
import { RegistroComponent } from './componentes/registro/registro.component';

// Servicios de la Aplicación
import { AutenticacionService } from './servicios/autenticacion.service';

// Rutas de la Aplicación
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './componentes/login/login.component';
import { PedidoComponent } from './componentes/pedido/pedido.component';
import { ResultadoComponent } from './componentes/resultado/resultado.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    LoginComponent,
    PedidoComponent,
    ResultadoComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [AutenticacionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
