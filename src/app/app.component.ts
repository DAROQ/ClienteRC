import { Component } from '@angular/core';
import { AutenticacionService } from './servicios/autenticacion.service';
import { Router, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public show: boolean = true;
  title = 'Asoft: Plazoleta de comidas';

  constructor(private autenticacionService:AutenticacionService, private router:Router) {
    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
      if(event.urlAfterRedirects == "/registro"){
        this.show = false;
      }else if(event.urlAfterRedirects == "/login"){
        this.show = false;
      }else if(event.urlAfterRedirects == "/pedido"){
        this.show = false;
      }else if(event.urlAfterRedirects == "/resultado"){
        this.show = false;
      }else {
        this.show = true;
      }
    });
    // this.autenticacionService.iniciarSesion("darocu","carcdc94").subscribe(data => {
    //   console.log(data);
    //   localStorage.setItem("token",data.token);
    // });
  }

   public logout(){
     localStorage.removeItem("token");
   }

}
